<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-183906442-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

    gtag('config', 'UA-183906442-1');
    gtag('config', 'AW-327007573');
</script>
<link rel="apple-touch-icon" href="/assets/images/apple.png">
<link rel="icon" href="/assets/images/favicon.png">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<?php wp_head(); 
	
	
	
	?>
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	
<script src='//eu.fw-cdn.com/10107292/72528.js' chat='false'></script>
</head>
<body  class="bkBlog">


	<?php
	if( get_locale() == 'nl_NL')
	{get_template_part( 'template-parts/hd_nl', get_post_type() );
	}
	else
	{
	get_template_part( 'template-parts/hd_en', get_post_type() );
	}
	?>
<div class="pg">	





