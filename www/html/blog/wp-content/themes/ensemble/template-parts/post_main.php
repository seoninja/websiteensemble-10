
	
	<div class="col-12">
				<div class="article">
					<div class="artImg "><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></a></div>
					<div class="artBody px-5 pb-5 pt-3">
					  <div class="details"><?php the_category( ' ' ); ?> <?php the_date(); ?></div>
						<h1 class="fw-bold"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
							<?php the_excerpt();?>
						<div class="author" style="background-image:url(/assets/images/author<?php echo get_the_author_meta( 'ID' );?>.png)">by <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );?>"><?php echo get_the_author(); ?></a></div>
					</div>
				
				</div>
			
			</div>